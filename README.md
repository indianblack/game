# The Royal Game of Ur

An Electron application with React and TypeScript implementing The Royal Game of Ur

## Project Setup

### Install

```bash
$ yarn install
```

### Development

```bash
$ yarn dev
```

### Build

```bash
# For windows
$ yarn build:win

# For macOS
$ yarn build:mac

# For Linux
$ yarn build:linux
```

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) + [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

## Packages & tools

- [Electron-vite](https://evite.netlify.app/)
- [Stitches](https://stitches.dev/)
- [Jotai](https://jotai.org/)
- [Immer](https://immerjs.github.io/immer/)
- [React-toastify](https://fkhadra.github.io/react-toastify/introduction)

## Useful links

- [Drag and drop with hooks - Draggable component](https://gist.github.com/crazypixel/98d44a666c41da819e21d15e2fabf64c)
- [React warning: findDOMNode is deprecated in StrictMode](https://www.kindacode.com/article/react-warning-finddomnode-is-deprecated-in-strictmode/)
- [Create draggable components with React-Draggable](https://blog.logrocket.com/create-draggable-components-react-draggable/)
- [How to extend CSSProperties in react project](https://stackoverflow.com/questions/50960084/how-to-extend-cssproperties-in-react-project)
- [Issue with inline style attributes on JSX component not assignable](https://github.com/microsoft/TypeScript/issues/18744)
- [Making Draggable Components in React](https://lo-victoria.com/making-draggable-components-in-react)
- [SvgComponent](https://react-svgr.com/playground/)
- [Move Element to Click Position](https://www.kirupa.com/snippets/move_element_to_click_position.htm)
- [Creating a Stateful Ref Object in React](https://medium.com/the-non-traditional-developer/creating-a-stateful-ref-object-in-react-fcd56d9dea58)
- [How to get the ref of the first element that's rendered with .map](https://stackoverflow.com/questions/47820031/how-to-get-the-ref-of-the-first-element-thats-rendered-with-map)
- [Empty items being added to array of refs in React](https://stackoverflow.com/questions/67687157/empty-items-being-added-to-array-of-refs-in-react)
- [Using addEventListener in Function components in React](https://bobbyhadz.com/blog/react-functional-component-add-event-listener)
- [Type void is not assignable to type event](https://stackoverflow.com/questions/51977823/type-void-is-not-assignable-to-type-event-mouseeventhtmlinputelement)
- [Removing an element with the plain JavaScript remove method](https://catalin.red/removing-an-element-with-plain-javascript-remove-method/)
- [Empty items being added to array of refs in React](https://stackoverflow.com/questions/67687157/empty-items-being-added-to-array-of-refs-in-react)
- [How to define an object of objects type in TypeScript](https://thewebdev.info/2022/03/20/how-to-define-an-object-of-objects-type-in-typescript/)
- [Coding a Turn-Based Battle Game With React Only](https://www.youtube.com/watch?v=r7Z7R25spkE)
- [atomWithImmer](https://github.com/pmndrs/jotai/blob/main/docs/integrations/immer.mdx)
- [Check if Array Contains an Object in JavaScript](https://bobbyhadz.com/blog/javascript-check-if-array-contains-object)
- [Immer update patterns](https://immerjs.github.io/immer/update-patterns/)
- [Make grid container fill columns not rows](https://stackoverflow.com/questions/44092529/make-grid-container-fill-columns-not-rows)
- [Box Shadow Generator](https://cssgenerator.pl/box-shadow-generator/)
- [How to Refresh a Page or Component in React](https://upmostly.com/tutorials/how-to-refresh-a-page-or-component-in-react)
- [Toast style is not working correctly](https://github.com/fkhadra/react-toastify/issues/202)
- [Using React-Toastify to style your toast messages](https://blog.logrocket.com/using-react-toastify-style-toast-messages/)
- [How can I fix the DevTools failed to load SourceMap: Could not load content error when adding a JavaScript library](https://stackoverflow.com/questions/61205390/how-can-i-fix-the-devtools-failed-to-load-sourcemap-could-not-load-content-er)
- [How to Migrate an existing React Web App to Desktop App with Electron](https://plainenglish.io/blog/migrate-existing-web-react-app-to-desktop-app-with-electron-a7007128120e#lets-code)
- [Electron and Vite.js with React.js](https://www.rocek.dev/blog/react_vite_a_electron)
- [Typescript Warning about Missing Return Type of function ESLint](https://stackoverflow.com/questions/54814753/typescript-warning-about-missing-return-type-of-function-eslint)
- [Disallow non-null assertions using the ! postfix operator](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/no-non-null-assertion.md)
- [Build Options - Vite](https://vitejs.dev/config/build-options.html#build-assetsinlinelimit)
- [Git replacing LF with CRLF](https://stackoverflow.com/questions/1967370/git-replacing-lf-with-crlf)

## License

Please contact [Białystok School of Economics](https://www.wse.edu.pl/)
