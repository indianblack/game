import {
  Border,
  Box,
  Button,
  CornerDecoration,
  Flex,
  Layout,
  MarkerBox,
  Section,
  Text
} from '../../components'
import { Card, HEIGHT, WIDTH } from './styles'

type EndGameViewProps = {
  playerOneScore: number
  playerTwoScore: number
}

const GameEndView = ({ playerOneScore, playerTwoScore }: EndGameViewProps) => {
  const winner: string = playerOneScore > playerTwoScore ? 'Player light' : 'Player dark'
  const markerBoxVariant: 'light' | 'dark' = playerOneScore > playerTwoScore ? 'light' : 'dark'
  return (
    <Layout>
      <Border
        css={{
          width: WIDTH,
          height: HEIGHT,
          minWidth: WIDTH,
          minHeight: HEIGHT
        }}
      >
        <Box
          css={{
            width: WIDTH,
            height: HEIGHT,
            minWidth: WIDTH,
            minHeight: HEIGHT
          }}
        >
          {/* Title */}
          <Section
            border="bottom"
            css={{
              width: WIDTH,
              minWidth: WIDTH
            }}
          >
            <CornerDecoration border="right" background="image" height="static" />
            <Flex>
              <Text size={4}>Congratulations!</Text>
            </Flex>
            <CornerDecoration border="left" background="image" height="static" />
          </Section>
          {/* Winner section */}
          <Flex>
            <CornerDecoration border="right" height="max" />
            <Flex>
              <Card>
                <MarkerBox player={markerBoxVariant} />
                <Text size={2} css={{ marginBottom: '$6' }}>
                  {winner} won!
                </Text>
              </Card>
            </Flex>
            <CornerDecoration border="left" height="max" />
          </Flex>
          {/* Footer */}
          <Section
            border="top"
            css={{
              width: WIDTH,
              minWidth: WIDTH
            }}
          >
            <CornerDecoration border="right" background="image" height="static" />
            <Flex>
              <Button onClick={() => window.location.reload()} css={{ width: '150px' }}>
                Play again
              </Button>
            </Flex>
            <CornerDecoration border="left" background="image" height="static" />
          </Section>
        </Box>
      </Border>
    </Layout>
  )
}
export default GameEndView
