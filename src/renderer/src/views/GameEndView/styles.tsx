import { styled } from '../../theme'

const WIDTH = '410px'
const HEIGHT = '339px'

const Card = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: '$1',
  backgroundColor: '$white90',
  width: '220px',
  height: '149px'
})

export { WIDTH, HEIGHT, Card }
