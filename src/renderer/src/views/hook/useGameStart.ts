import { SetStateAction, useEffect } from 'react'
import { Player } from '../constants/players'
import { DiceRoll, GameStage, Message } from '../constants/types'

export const useGameStart = (
  stage: GameStage,
  setTurn: (update: SetStateAction<Player>) => void,
  playerOneDiceRoll: DiceRoll | undefined,
  playerTwoDiceRoll: DiceRoll | undefined,
  setMessage: (update: SetStateAction<string>) => void,
  setMessageType: (update: SetStateAction<Message>) => void
) => {
  useEffect(() => {
    if (stage !== GameStage.Start) return

    if (playerOneDiceRoll !== undefined && playerTwoDiceRoll === undefined) {
      setTurn(Player.Two)
      return
    }

    if (playerOneDiceRoll === undefined || playerTwoDiceRoll === undefined) {
      setMessage('Roll to check which player starts')
      return
    }

    if (playerOneDiceRoll?.total === playerTwoDiceRoll?.total) {
      setMessage("It's a match! Roll again")
      setMessageType(Message.Important)
      return
    }

    if (playerOneDiceRoll?.total === null || playerTwoDiceRoll?.total === null) return

    if (playerOneDiceRoll?.total > playerTwoDiceRoll?.total) {
      setTurn(Player.One)
      setMessageType(Message.Action)
      setMessage('Player light starts!')
    } else {
      setTurn(Player.Two)
      setMessageType(Message.Action)
      setMessage('Player dark starts!')
    }
  }, [stage, playerOneDiceRoll, playerTwoDiceRoll])
}
