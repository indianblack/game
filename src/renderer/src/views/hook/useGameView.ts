import produce from 'immer'
import { useAtom } from 'jotai'
import { useEffect, useRef } from 'react'
import {
  Player,
  PLAYER_ONE_LAST_TILE,
  PLAYER_TWO_LAST_TILE,
  TILES_PLAYER_ONE,
  TILES_PLAYER_TWO
} from '../constants/players'
import { TILES } from '../constants/tiles'
import { GameStage, Message } from '../constants/types'
import {
  playerOneDiceRollAtom,
  playerTwoDiceRollAtom,
  stageAtom,
  turnAtom,
  markerLightCountAtom,
  playerOneMarkersPositionAtom,
  playerOneScoreAtom,
  markerDarkCountAtom,
  playerTwoMarkersPositionAtom,
  playerTwoScoreAtom,
  markerOnRosetteTileAtom,
  hoverAtom,
  hoveredMarkerIndexAtom,
  messageAtom,
  messageTypeAtom
} from '../state/gameState'
import { notify } from '../utils/toast'
import {
  checkIfLastTile,
  checkIfTileIsTaken,
  checkPlayerRoll,
  compareRollAndMove,
  takeDownOpponentMarker,
  uncheckTiles,
  useGameMiddle,
  checkIfRosetteTile
} from './useGameMiddle'
import { useGameStart } from './useGameStart'

const BORDER_STYLE = 'thick dotted #EC1616'

export const useGameView = () => {
  // State
  const [stage, setStage] = useAtom(stageAtom)
  const [turn, setTurn] = useAtom(turnAtom)
  const [playerOneDiceRoll, setPlayerOneDiceRoll] = useAtom(playerOneDiceRollAtom)
  const [playerTwoDiceRoll, setPlayerTwoDiceRoll] = useAtom(playerTwoDiceRollAtom)
  const [markerLightCount, setMarkerLightCount] = useAtom(markerLightCountAtom)
  const [playerOneMarkersPosition, setPlayerOneMarkersPosition] = useAtom(
    playerOneMarkersPositionAtom
  )
  const [playerOneScore, setPlayerOneScore] = useAtom(playerOneScoreAtom)
  const [playerTwoScore, setPlayerTwoScore] = useAtom(playerTwoScoreAtom)
  const [markerDarkCount, setMarkerDarkCount] = useAtom(markerDarkCountAtom)
  const [playerTwoMarkersPosition, setPlayerTwoMarkersPosition] = useAtom(
    playerTwoMarkersPositionAtom
  )
  const [hover, setHover] = useAtom(hoverAtom)
  const [hoveredMarkerIndex, setHoveredMarkerIndex] = useAtom(hoveredMarkerIndexAtom)
  const [markerOnRosetteTile, setMarkerOnRosetteTile] = useAtom(markerOnRosetteTileAtom)
  const [message, setMessage] = useAtom(messageAtom)
  const [messageType, setMessageType] = useAtom(messageTypeAtom)

  // Refs
  const tilesRef = useRef<HTMLDivElement[]>([])
  const markersLightRef = useRef<HTMLButtonElement[]>([])
  const markersDarkRef = useRef<HTMLButtonElement[]>([])
  const oneOuterRef = useRef<HTMLDivElement>(null)
  const twoOuterRef = useRef<HTMLDivElement>(null)

  // Toggle hover
  const handleMouseIn = (hoveredMarkerIndex: number) => {
    setHover(true)
    setHoveredMarkerIndex(hoveredMarkerIndex)
  }

  const handleMouseOut = () => {
    setHover(false)
  }

  // Roll dice
  const rollDice = () => {
    let total = 0
    const singleDraw: number[] = []

    for (let i = 0; i < 4; i++) {
      const randomNumber = Math.floor(Math.random() * 2)
      singleDraw.push(randomNumber)
      total = total + randomNumber
    }

    if (turn === Player.One) {
      setPlayerOneDiceRoll({ draw: singleDraw, total: total })
    }

    if (turn === Player.Two) {
      setPlayerTwoDiceRoll({ draw: singleDraw, total: total })
    }
  }

  // When it's a match at start game
  const match = () => {
    setMessageType(Message.Normal)
    setTurn(Player.One)
    setPlayerOneDiceRoll(undefined)
    setPlayerTwoDiceRoll(undefined)
  }

  // When one player begins at start game
  const play = () => {
    setMessage('')
    setMessageType(Message.Normal)
    setPlayerOneDiceRoll(undefined)
    setPlayerTwoDiceRoll(undefined)
    setStage(GameStage.Middle)
  }

  // Show available moves on hover
  const deriveAvailableMove = (markerIndex: number): number[] => {
    let availableMove: number[] = [] // Available move will be different depending on the player
    if (turn === Player.One) {
      if (checkPlayerRoll(playerOneDiceRoll)) return availableMove // Player needs to roll the dice
      if (markerIndex === undefined) return availableMove // Hover any marker

      const marker = playerOneMarkersPosition.filter((el) => {
        if (el === undefined) return
        if (el.markerIndex !== markerIndex) return
        return el
      })
      const currentTile = TILES_PLAYER_ONE.indexOf(marker[0].tileIndex) + 1
      const nextTiles = currentTile + playerOneDiceRoll!.total!
      availableMove = TILES_PLAYER_ONE.slice(currentTile, nextTiles) // Get tile indexes
    }

    if (turn === Player.Two) {
      if (checkPlayerRoll(playerTwoDiceRoll)) return availableMove // Player needs to roll the dice
      if (markerIndex === undefined) return availableMove // Hover any marker

      const marker = playerTwoMarkersPosition.filter((el) => {
        if (el === undefined) return
        if (el.markerIndex !== markerIndex) return
        return el
      })
      const currentTile = TILES_PLAYER_TWO.indexOf(marker[0].tileIndex) + 1
      const nextTiles = currentTile + playerTwoDiceRoll!.total!
      availableMove = TILES_PLAYER_TWO.slice(currentTile, nextTiles) // Get tile indexes
    }

    return availableMove
  }

  useEffect(() => {
    if (hoveredMarkerIndex === undefined) return
    const availableMove = deriveAvailableMove(hoveredMarkerIndex) // Get available move

    if (turn === Player.One) {
      if (checkPlayerRoll(playerOneDiceRoll)) return // If the roll wasn't made
      if (compareRollAndMove(availableMove, playerOneDiceRoll)) return // If the marker is near the end but the total roll is higher
      if (checkIfTileIsTaken(availableMove, tilesRef, playerTwoMarkersPosition, playerOneDiceRoll))
        return // If tile is taken by current player marker

      availableMove.forEach((tile) => {
        tilesRef.current[tile].style.border = BORDER_STYLE
      }) // If hover - add border to tiles
    }

    if (turn === Player.Two) {
      if (checkPlayerRoll(playerTwoDiceRoll)) return // If the roll wasn't made
      if (compareRollAndMove(availableMove, playerTwoDiceRoll)) return // If the marker is near the end but the total roll is higher
      if (checkIfTileIsTaken(availableMove, tilesRef, playerOneMarkersPosition, playerTwoDiceRoll))
        return // If tile is taken by current player marker

      availableMove.forEach((tile) => {
        tilesRef.current[tile].style.border = BORDER_STYLE
      }) // If hover - add border to tiles
    }

    if (hover === false) {
      uncheckTiles(tilesRef)
    } // Remove additional styling from tiles
  }, [hover])

  const moveMarker = (markerIndex: number) => {
    if (hoveredMarkerIndex === undefined) return
    const availableMove = deriveAvailableMove(hoveredMarkerIndex) // Get available move
    const targetTile = availableMove.slice(-1)[0] // Get the target tile

    if (turn === Player.One) {
      const isTileTakenByCurrentPlayer: boolean = checkIfTileIsTaken(
        availableMove,
        tilesRef,
        playerTwoMarkersPosition,
        playerOneDiceRoll
      )
      if (checkPlayerRoll(playerOneDiceRoll)) return // If the roll wasn't made
      if (compareRollAndMove(availableMove, playerOneDiceRoll)) return // If the marker is near the end but the total roll is higher
      if (isTileTakenByCurrentPlayer) return // If tile is taken by current player marker
      if (
        checkIfLastTile(
          targetTile,
          markerIndex,
          PLAYER_ONE_LAST_TILE,
          markersLightRef,
          setPlayerOneScore,
          playerOneScore,
          setPlayerOneMarkersPosition,
          setPlayerOneDiceRoll,
          setHover,
          markerOnRosetteTile,
          setMarkerOnRosetteTile,
          tilesRef,
          turn,
          setTurn
        )
      ) {
        notify(turn, 'score') // Toast
        return
      } // If it's the last tile remove the marker and don't move

      if (isTileTakenByCurrentPlayer === false) {
        takeDownOpponentMarker(
          targetTile,
          playerTwoMarkersPosition,
          tilesRef,
          markersDarkRef,
          setPlayerTwoMarkersPosition,
          markerDarkCount,
          setMarkerDarkCount,
          twoOuterRef,
          Player.Two
        )
      } // Remove oponent marker

      markersLightRef.current[markerIndex].parentNode!.removeChild(
        markersLightRef.current[markerIndex]
      ) // Remove marker from the current tile

      tilesRef.current[targetTile].append(markersLightRef.current[markerIndex]) // Move marker to the the target tile

      setPlayerOneMarkersPosition((prev) =>
        produce(prev, (draft) => {
          if (draft[markerIndex] !== undefined) {
            draft[markerIndex].tileIndex = targetTile
          } else {
            draft.push({
              markerIndex: markerIndex,
              tileIndex: targetTile
            })
          }
        })
      ) // Update position state
      setPlayerOneDiceRoll(undefined) // Clear roll
    }

    if (turn === Player.Two) {
      const isTileTakenByCurrentPlayer: boolean = checkIfTileIsTaken(
        availableMove,
        tilesRef,
        playerOneMarkersPosition,
        playerTwoDiceRoll
      )
      if (checkPlayerRoll(playerTwoDiceRoll)) return // If the roll wasn't made
      if (compareRollAndMove(availableMove, playerTwoDiceRoll)) return // If the marker is near the end but the total roll is higher
      if (isTileTakenByCurrentPlayer) return // If tile is taken by current player marker
      if (
        checkIfLastTile(
          targetTile,
          markerIndex,
          PLAYER_TWO_LAST_TILE,
          markersDarkRef,
          setPlayerTwoScore,
          playerTwoScore,
          setPlayerTwoMarkersPosition,
          setPlayerTwoDiceRoll,
          setHover,
          markerOnRosetteTile,
          setMarkerOnRosetteTile,
          tilesRef,
          turn,
          setTurn
        )
      ) {
        notify(turn, 'score') // Toast
        return
      } // If it's the last tile remove the marker and don't move

      if (isTileTakenByCurrentPlayer === false) {
        takeDownOpponentMarker(
          targetTile,
          playerOneMarkersPosition,
          tilesRef,
          markersLightRef,
          setPlayerOneMarkersPosition,
          markerLightCount,
          setMarkerLightCount,
          oneOuterRef,
          Player.One
        )
      } // Remove oponent marker

      markersDarkRef.current[markerIndex].parentNode!.removeChild(
        markersDarkRef.current[markerIndex]
      ) // Remove marker from the current tile

      tilesRef.current[targetTile].append(markersDarkRef.current[markerIndex]) // Move marker to the the target tile

      setPlayerTwoMarkersPosition((prev) =>
        produce(prev, (draft) => {
          if (draft[markerIndex] !== undefined) {
            draft[markerIndex].tileIndex = targetTile
          } else {
            draft.push({
              markerIndex: markerIndex,
              tileIndex: targetTile
            })
          }
        })
      ) // Update position state
      setPlayerTwoDiceRoll(undefined) // Clear roll
    }

    setHover(false) // Update hover state
    uncheckTiles(tilesRef) // Remove additional styling from tiles
    if (checkIfRosetteTile(targetTile, markerOnRosetteTile, setMarkerOnRosetteTile) === true) {
      notify(turn, 'markerOnRosette') // Toast
      setPlayerOneDiceRoll(undefined)
      setPlayerTwoDiceRoll(undefined)
      return
    } // If target tile is rosette - player can roll dice again. Don't change turn.
    setMarkerOnRosetteTile(false) // Player already had marker on rosette and moved twice - update state for next player
    setTurn((prev) => (prev === Player.One ? Player.Two : Player.One)) // Change turn when move ends
  }

  // On game start
  useGameStart(stage, setTurn, playerOneDiceRoll, playerTwoDiceRoll, setMessage, setMessageType)

  // On game middle
  useGameMiddle(
    stage,
    turn,
    setTurn,
    markersLightRef,
    markersDarkRef,
    tilesRef,
    playerOneDiceRoll,
    setPlayerOneDiceRoll,
    playerTwoDiceRoll,
    setPlayerTwoDiceRoll,
    markerLightCount,
    setMarkerLightCount,
    playerOneMarkersPosition,
    setPlayerOneMarkersPosition,
    markerDarkCount,
    setMarkerDarkCount,
    playerTwoMarkersPosition,
    setPlayerTwoMarkersPosition,
    deriveAvailableMove,
    playerOneScore,
    playerTwoScore,
    setStage,
    notify,
    setMarkerOnRosetteTile
  )

  return {
    rollDice,
    turn,
    stage,
    TILES,
    tilesRef,
    markersLightRef,
    markersDarkRef,
    oneOuterRef,
    twoOuterRef,
    handleMouseIn,
    handleMouseOut,
    moveMarker,
    playerOneScore,
    playerTwoScore,
    message,
    messageType,
    match,
    play,
    playerOneDiceRoll,
    playerTwoDiceRoll
  }
}
