import produce from 'immer'
import { SetStateAction, useEffect } from 'react'
import { Player, TILES_PLAYER_ONE, TILES_PLAYER_TWO } from '../constants/players'
import { DiceRoll, GameStage, MarkerPosition, ToastMessageVariant } from '../constants/types'
import { MIDDLE_ROSETTE, ROSETTE_TILES } from '../constants/tiles'
import { notify } from '../utils/toast'

/* 
Check if player rolled the dice
*/
export const checkPlayerRoll = (diceRoll: DiceRoll | undefined): boolean => {
  return diceRoll === undefined || diceRoll.total === undefined || diceRoll.total === null
}

/* 
If player marker is near the end but the total roll is higher
player should not be able to move the marker
*/
export const compareRollAndMove = (
  availableMove: number[],
  diceRoll: DiceRoll | undefined
): boolean => {
  if (diceRoll == undefined || diceRoll.total === undefined || diceRoll.total === null) return false
  return availableMove.length < diceRoll.total
}

/* 
Check if tile is taken by current player's marker
or by oponent's marker
*/
export const checkIfTileIsTaken = (
  availableMove: number[],
  tilesRef: React.MutableRefObject<HTMLDivElement[]>,
  playerMarkerPosition: MarkerPosition[],
  diceRoll: DiceRoll | undefined
): boolean => {
  if (diceRoll == undefined) return true
  const targetTile: number = availableMove.slice(-1)[0] // Get last tile from available move
  const isMarker: boolean = tilesRef.current[targetTile].hasChildNodes() // Check if the tile is taken
  let isMarkerOfAnotherPlayer = false // Check if tile is taken by another user
  const isOnRosetteTile: boolean = targetTile === MIDDLE_ROSETTE // Check if target tile is the middle rosette

  const whereIsOtherPlayer = Object.values(playerMarkerPosition).map((el) => {
    if (el === undefined) return
    return el.tileIndex // After removing a marker from state (when marker finished), it's undefined in playerOneMarkersPosition
  })
  isMarkerOfAnotherPlayer = whereIsOtherPlayer.includes(targetTile)

  if (isMarkerOfAnotherPlayer && isOnRosetteTile === false) return false // If it's another player marker, move is available if the tile is not middle rosette
  return isMarker // If it's current player marker - don't move
}

/* 
Remove additional styling from tiles
*/
export const uncheckTiles = (tilesRef: React.MutableRefObject<HTMLDivElement[]>) => {
  tilesRef.current.forEach((tile) => {
    tile.style.border = 'none'
  })
}

/* 
Check if it's the last tile
*/
export const checkIfLastTile = (
  nextTile: number,
  markerIndex: number,
  playerLastTile: number,
  markersRef: React.MutableRefObject<HTMLButtonElement[]>,
  setPlayerScore: (update: SetStateAction<number>) => void,
  playerCurrentScore: number,
  setPlayerMarkersPosition: (update: SetStateAction<MarkerPosition[]>) => void,
  setPlayerDiceRoll: (update?: SetStateAction<DiceRoll | undefined>) => void,
  setHover: (update: SetStateAction<boolean>) => void,
  markerOnRosetteTile: boolean,
  setMarkerOnRosetteTile: (update: SetStateAction<boolean>) => void,
  tilesRef: React.MutableRefObject<HTMLDivElement[]>,
  turn: Player,
  setTurn: (update: SetStateAction<Player>) => void
): boolean => {
  if (nextTile !== playerLastTile) return false // If it's not the last tile - return false

  markersRef.current[markerIndex].parentNode!.removeChild(markersRef.current[markerIndex]) // Remove the marker from the game

  setPlayerScore(playerCurrentScore + 1) // Update player score

  setPlayerMarkersPosition((prev) =>
    produce(prev, (draft) => {
      delete draft[markerIndex]
    })
  ) // Remove marker from state

  setPlayerDiceRoll(undefined) // Clear roll
  setHover(false) // Update hover state

  if (markerOnRosetteTile === false) {
    notify(turn, 'markerOnRosette') // Toast
    setMarkerOnRosetteTile(true)
  } // If marker stands on last rosette - player gets an additional move

  if (markerOnRosetteTile === true) {
    setMarkerOnRosetteTile(false)
    setTurn((prev) => (prev === Player.One ? Player.Two : Player.One)) // Change turn when move ends
  } // If two markers end one by one - the player should have only one more available move

  uncheckTiles(tilesRef) // Remove additional styling from tiles

  return true
}

/* 
Remove oponent marker
*/
export const takeDownOpponentMarker = (
  tileIndex: number,
  otherPlayerMarkersPosition: MarkerPosition[],
  tilesRef: React.MutableRefObject<HTMLDivElement[]>,
  otherPlayerMarkersRef: React.MutableRefObject<HTMLButtonElement[]>,
  setOtherPlayerMarkersPosition: (update: SetStateAction<MarkerPosition[]>) => void,
  otherPlayerMarkerCount: number[],
  setOtherPlayerMarkerCount: (update: SetStateAction<number[]>) => void,
  outerRef: React.RefObject<HTMLDivElement>,
  otherPlayer: Player
) => {
  const opponentMarker = Object.values(otherPlayerMarkersPosition).filter((el) => {
    if (el === undefined) return
    return el.tileIndex === tileIndex
  })
  if (opponentMarker === undefined || opponentMarker.length === 0) return
  if (opponentMarker[0] === undefined) return

  const opponentMarkerIndex = opponentMarker[0].markerIndex
  tilesRef.current[tileIndex].removeChild(otherPlayerMarkersRef.current[opponentMarkerIndex]) // Remove opponent's marker from the current tile

  setOtherPlayerMarkersPosition((prev) =>
    produce(prev, (draft) => {
      delete draft[opponentMarkerIndex]
    })
  ) // Remove marker from state

  const newMarkerCount = [...otherPlayerMarkerCount, opponentMarkerIndex].sort()
  setOtherPlayerMarkerCount(newMarkerCount) // Update marker count

  outerRef.current!.append(otherPlayerMarkersRef.current[opponentMarkerIndex]) // Move marker back to que
  notify(otherPlayer, 'markerDown') // Toast
}

/* 
Check if marker is on rosette tile
If marker of a given player once stand on rosette
he has an additional move - but only one time
*/
export const checkIfRosetteTile = (
  nextTile: number,
  markerOnRosetteTile: boolean,
  setMarkerOnRosetteTile: (update: SetStateAction<boolean>) => void
): boolean => {
  if (markerOnRosetteTile) return false

  setMarkerOnRosetteTile(true)
  return ROSETTE_TILES.includes(nextTile)
}

/* 
Game middle
*/
export const useGameMiddle = (
  stage: GameStage,
  turn: Player,
  setTurn: (update: SetStateAction<Player>) => void,
  markersLightRef: React.MutableRefObject<HTMLButtonElement[]>,
  markersDarkRef: React.MutableRefObject<HTMLButtonElement[]>,
  tilesRef: React.MutableRefObject<HTMLDivElement[]>,
  playerOneDiceRoll: DiceRoll | undefined,
  setPlayerOneDiceRoll: (update?: SetStateAction<DiceRoll | undefined>) => void,
  playerTwoDiceRoll: DiceRoll | undefined,
  setPlayerTwoDiceRoll: (update?: SetStateAction<DiceRoll | undefined>) => void,
  markerLightCount: number[],
  setMarkerLightCount: (update: SetStateAction<number[]>) => void,
  playerOneMarkersPosition: MarkerPosition[],
  setPlayerOneMarkersPosition: (update: SetStateAction<MarkerPosition[]>) => void,
  markerDarkCount: number[],
  setMarkerDarkCount: (update: SetStateAction<number[]>) => void,
  playerTwoMarkersPosition: MarkerPosition[],
  setPlayerTwoMarkersPosition: (update: SetStateAction<MarkerPosition[]>) => void,
  deriveAvailableMove: (markerIndex: number) => number[],
  playerOneScore: number,
  playerTwoScore: number,
  setStage: (update: SetStateAction<GameStage>) => void,
  notify: (player: Player, messageVariant: ToastMessageVariant) => void,
  setMarkerOnRosetteTile: (update: SetStateAction<boolean>) => void
) => {
  const addNextMarker = () => {
    if (turn === Player.One) {
      if (markersLightRef.current === null) return
      if (markerLightCount.length === 0) return // If there's no markers left - stop
      const startIndexPlayerOne = TILES_PLAYER_ONE[0]
      const startTilePlayerOne = tilesRef.current[startIndexPlayerOne]
      if (startTilePlayerOne.childNodes.length > 0) return // If there's already a marker on position 0 - stop

      const markerIndex = markerLightCount[0]
      setMarkerLightCount(markerLightCount.slice(1))

      markersLightRef.current[markerIndex].parentNode!.removeChild(
        markersLightRef.current[markerIndex]
      ) // Remove marker from the que
      startTilePlayerOne.append(markersLightRef.current[markerIndex]) // Add marker in start position

      setPlayerOneMarkersPosition((prev) =>
        produce(prev, (draft) => {
          if (prev[markerIndex] === undefined) {
            draft[markerIndex] = {
              markerIndex: markerIndex,
              tileIndex: startIndexPlayerOne
            }
          } else {
            draft.push({
              markerIndex: markerIndex,
              tileIndex: startIndexPlayerOne
            })
          }
        })
      ) // Update position state
    }

    if (turn === Player.Two) {
      if (markersDarkRef.current === null) return
      if (markerDarkCount.length === 0) return // If there's no markers left - stop
      const startIndexPlayerTwo = TILES_PLAYER_TWO[0]
      const startTilePlayerTwo = tilesRef.current[startIndexPlayerTwo]
      if (startTilePlayerTwo.childNodes.length > 0) return // If there's already a marker on position 0 - stop

      const markerIndex = markerDarkCount[0]
      setMarkerDarkCount(markerDarkCount.slice(1))

      markersDarkRef.current[markerIndex].parentNode!.removeChild(
        markersDarkRef.current[markerIndex]
      ) // Remove marker from the que
      startTilePlayerTwo.append(markersDarkRef.current[markerIndex]) // Add marker in start position

      setPlayerTwoMarkersPosition((prev) =>
        produce(prev, (draft) => {
          if (prev[markerIndex] === undefined) {
            draft[markerIndex] = {
              markerIndex: markerIndex,
              tileIndex: startIndexPlayerTwo
            }
          } else {
            draft.push({
              markerIndex: markerIndex,
              tileIndex: startIndexPlayerTwo
            })
          }
        })
      ) // Update position state
    }
  }

  const isAnyMoveAvailable = (): boolean => {
    const isMoveAvailable: boolean[] = []
    if (turn === Player.One) {
      if (
        playerOneDiceRoll === undefined ||
        playerOneDiceRoll.total === undefined ||
        playerOneDiceRoll.total === null
      )
        return false // should be use effect on dice roll
      if (playerOneMarkersPosition.length === 0) return true // If there's no markers in the game, the move is still available

      playerOneMarkersPosition.forEach((marker: MarkerPosition) => {
        if (marker === undefined) return
        const availableMove = deriveAvailableMove(marker.markerIndex)

        const isMoveEqualToRoll = availableMove.length === playerOneDiceRoll.total!
        const isTileTaken = checkIfTileIsTaken(
          availableMove,
          tilesRef,
          playerTwoMarkersPosition,
          playerOneDiceRoll
        )

        isMoveEqualToRoll === true && isTileTaken === false
          ? isMoveAvailable.push(true)
          : isMoveAvailable.push(false)
        // If the marker is near the end but the total roll is higher and if tile is taken by another marker
        // move is not available, otherwise it's available
      })
    }

    if (turn === Player.Two) {
      if (
        playerTwoDiceRoll === undefined ||
        playerTwoDiceRoll.total === undefined ||
        playerTwoDiceRoll.total === null
      )
        return false // should be use effect on dice roll
      if (playerTwoMarkersPosition.length === 0) return true // If there's no markers in ten game, the move is still available

      playerTwoMarkersPosition.forEach((marker: MarkerPosition) => {
        if (marker === undefined) return
        const availableMove = deriveAvailableMove(marker.markerIndex)

        const isMoveEqualToRoll = availableMove.length === playerTwoDiceRoll.total!
        const isTileTaken = checkIfTileIsTaken(
          availableMove,
          tilesRef,
          playerOneMarkersPosition,
          playerTwoDiceRoll
        )

        isMoveEqualToRoll === true && isTileTaken === false
          ? isMoveAvailable.push(true)
          : isMoveAvailable.push(false)
        // If the marker is near the end but the total roll is higher and if tile is taken by another marker
        // move is not available, otherwise it's available
      })
    }

    return isMoveAvailable.includes(true)
  }

  useEffect(() => {
    if (stage !== GameStage.Middle) return

    if (turn === Player.One) {
      if (playerOneDiceRoll === undefined) return
      if (playerOneDiceRoll.total === 0) {
        notify(Player.One, 'rollZero') // Toast
        setMarkerOnRosetteTile(false)
        setPlayerOneDiceRoll(undefined) // Clear roll
        setTurn(Player.Two)
        return
      } // If player roll
      addNextMarker()
      if (playerOneDiceRoll.total !== 0 && isAnyMoveAvailable() === false) {
        notify(Player.One, 'noMoveAvailable') // Toast
        setPlayerOneDiceRoll(undefined) // Clear roll
        setTurn(Player.Two)
        return
      } // If there's no available move for any marker
    }

    if (turn === Player.Two) {
      if (playerTwoDiceRoll === undefined) return
      if (playerTwoDiceRoll.total === 0) {
        notify(Player.Two, 'rollZero') // Toast
        setMarkerOnRosetteTile(false)
        setPlayerTwoDiceRoll(undefined) // Clear roll
        setTurn(Player.One)
        return
      } // If player roll === 0 change turn
      addNextMarker()
      if (playerTwoDiceRoll.total !== 0 && isAnyMoveAvailable() === false) {
        notify(Player.Two, 'noMoveAvailable') // Toast
        setPlayerTwoDiceRoll(undefined) // Clear roll
        setTurn(Player.One)
        return
      } // If there's no available move for any marker, change turn
    }
  }, [
    turn,
    playerOneDiceRoll,
    playerTwoDiceRoll,
    playerOneMarkersPosition,
    playerTwoMarkersPosition
  ])

  // Keep track of the score to see who's winning
  useEffect(() => {
    if (stage !== GameStage.Middle) return
    if (playerOneScore === 6 || playerTwoScore === 6) setStage(GameStage.End)
  }, [playerOneScore, playerTwoScore])
}
