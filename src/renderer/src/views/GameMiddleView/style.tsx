import { styled } from '../../theme'

const SECTION_WIDTH = '167px'

const ViewWrapper = styled('div', {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row',
  height: '100vh',
  width: '100vw'
})

const GameWrapper = styled('div', {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row',
  height: '600px',
  width: '1200px'
})

const PlayerSectionWrapper = styled('div', {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  flexDirection: 'column',
  height: '552px', // GAME_BASIS_HEIGHT
  variants: {
    player: {
      light: {
        marginRight: '$9'
      },
      dark: {
        marginLeft: '$9'
      }
    }
  }
})

const PlayerCard = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  width: SECTION_WIDTH,
  padding: '$2',
  borderRadius: '$1',
  variants: {
    player: {
      light: {
        backgroundColor: '$white80'
      },
      dark: {
        backgroundColor: '$blue80'
      }
    }
  }
})

const PlayerCardBorder = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: '$1',
  border: '0.5px solid $beige',
  width: '100%',
  height: '100%',
  padding: '$6',
  gap: '$5'
})

const TitleWrapper = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  gap: '0'
})

const ButtonWrapper = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
  gap: '$2'
})

const MarkerSectionOverlay = styled('div', {
  position: 'absolute',
  right: '0',
  width: SECTION_WIDTH,
  height: '92px',
  backgroundColor: 'transparent'
})

const MarkerSection = styled('div', {
  width: '144px',
  position: 'relative',
  display: 'flex',
  justifyContent: 'flex-end',
  alignItems: 'flex-end',
  gap: '$4',
  variants: {
    direction: {
      right: {
        flexWrap: 'wrap-reverse'
      },
      left: {
        flexWrap: 'wrap-reverse',
        flexDirection: 'row-reverse'
      }
    }
  }
})

export {
  ViewWrapper,
  GameWrapper,
  PlayerSectionWrapper,
  MarkerSection,
  MarkerSectionOverlay,
  PlayerCard,
  PlayerCardBorder,
  TitleWrapper,
  ButtonWrapper
}
