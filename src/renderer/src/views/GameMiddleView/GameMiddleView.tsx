import { DiceRoll, TileElement } from '../constants/types'
import { Player } from '../constants/players'
import { markers } from '../constants/markers'
import { Badge, Basis, Button, Marker, Text } from '../../components'
import {
  ButtonWrapper,
  GameWrapper,
  MarkerSection,
  MarkerSectionOverlay,
  PlayerCard,
  PlayerCardBorder,
  PlayerSectionWrapper,
  TitleWrapper,
  ViewWrapper
} from './style'

type MiddleGameViewProps = {
  onRollDice: (playerId: Player) => void
  turn: string
  tiles: TileElement[]
  tilesRef: React.MutableRefObject<HTMLDivElement[]>
  markersLightRef: React.MutableRefObject<HTMLButtonElement[]>
  markersDarkRef: React.MutableRefObject<HTMLButtonElement[]>
  oneOuterRef: React.RefObject<HTMLDivElement>
  twoOuterRef: React.RefObject<HTMLDivElement>
  onHandleMouseIn: (hoveredMarkerIndex: number) => void
  onHandleMouseOut: () => void
  onMoveMarker: (clickedMarkerIndex: number) => void
  playerOneDiceRoll: DiceRoll | undefined
  playerTwoDiceRoll: DiceRoll | undefined
  playerOneScore: number
  playerTwoScore: number
}

const GameMiddleView = ({
  onRollDice,
  turn,
  tiles,
  tilesRef,
  markersLightRef,
  markersDarkRef,
  oneOuterRef,
  twoOuterRef,
  onHandleMouseIn,
  onHandleMouseOut,
  onMoveMarker,
  playerOneDiceRoll,
  playerTwoDiceRoll,
  playerOneScore,
  playerTwoScore
}: MiddleGameViewProps) => {
  return (
    <ViewWrapper>
      <GameWrapper>
        {/* Player light section */}
        <PlayerSectionWrapper player="light">
          <PlayerCard player="light">
            <PlayerCardBorder>
              <TitleWrapper>
                <Text size="1">Player</Text>
                <Text size="4">Light</Text>
              </TitleWrapper>
              <ButtonWrapper>
                <Badge color="light" css={{ width: '100%' }}>
                  <Text>Score: {playerOneScore}</Text>
                </Badge>
                {playerOneDiceRoll !== undefined && (
                  <Badge color="dark" css={{ width: '100%' }}>
                    <Text>Move: {playerOneDiceRoll.total}</Text>
                  </Badge>
                )}
                {playerOneDiceRoll === undefined && (
                  <Button
                    onClick={() => onRollDice(Player.One)}
                    disabled={turn === Player.Two ? true : false}
                  >
                    Roll
                  </Button>
                )}
              </ButtonWrapper>
            </PlayerCardBorder>
          </PlayerCard>
          <MarkerSection ref={oneOuterRef} direction="left">
            <MarkerSectionOverlay />
            {markers.map((index) => {
              return (
                <Marker
                  color="light"
                  key={index}
                  index={index}
                  disabled={turn === Player.Two}
                  markersRef={markersLightRef}
                  onHandleMouseIn={(index: number) => onHandleMouseIn(index)}
                  onHandleMouseOut={onHandleMouseOut}
                  onMoveMarker={onMoveMarker}
                />
              )
            })}
          </MarkerSection>
        </PlayerSectionWrapper>
        {/* Board section */}
        <Basis tiles={tiles} tilesRef={tilesRef} />
        {/* Player dark section */}
        <PlayerSectionWrapper player="dark">
          <PlayerCard player="dark">
            <PlayerCardBorder>
              <TitleWrapper>
                <Text size="1" color="white">
                  Player
                </Text>
                <Text size="4" color="white">
                  Dark
                </Text>
              </TitleWrapper>
              <ButtonWrapper>
                <Badge color="light" css={{ width: '100%' }}>
                  <Text>Score: {playerTwoScore}</Text>
                </Badge>
                {playerTwoDiceRoll !== undefined && (
                  <Badge color="dark" css={{ width: '100%' }}>
                    <Text>Move: {playerTwoDiceRoll.total}</Text>
                  </Badge>
                )}
                {playerTwoDiceRoll === undefined && (
                  <Button
                    onClick={() => onRollDice(Player.Two)}
                    disabled={turn === Player.One ? true : false}
                  >
                    Roll
                  </Button>
                )}
              </ButtonWrapper>
            </PlayerCardBorder>
          </PlayerCard>
          <MarkerSection ref={twoOuterRef} direction="right">
            <MarkerSectionOverlay />
            {markers.map((index) => {
              return (
                <Marker
                  color="dark"
                  key={index}
                  index={index}
                  disabled={turn === Player.One}
                  markersRef={markersDarkRef}
                  onHandleMouseIn={(index: number) => onHandleMouseIn(index)}
                  onHandleMouseOut={onHandleMouseOut}
                  onMoveMarker={onMoveMarker}
                />
              )
            })}
          </MarkerSection>
        </PlayerSectionWrapper>
      </GameWrapper>
    </ViewWrapper>
  )
}
export default GameMiddleView
