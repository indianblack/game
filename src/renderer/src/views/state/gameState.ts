import { atom } from 'jotai'
import { Player } from '../constants/players'
import { DiceRoll, GameStage, MarkerPosition, Message } from '../constants/types'

export const stageAtom = atom<GameStage>(GameStage.Start)

export const playerOneDiceRollAtom = atom<DiceRoll | undefined>(undefined)

export const playerTwoDiceRollAtom = atom<DiceRoll | undefined>(undefined)

export const turnAtom = atom<Player>(Player.One)

export const markerLightCountAtom = atom<number[]>([0, 1, 2, 3, 4, 5])

export const playerOneMarkersPositionAtom = atom<MarkerPosition[]>([])

export const playerOneScoreAtom = atom<number>(0)

export const markerDarkCountAtom = atom<number[]>([0, 1, 2, 3, 4, 5])

export const playerTwoMarkersPositionAtom = atom<MarkerPosition[]>([])

export const playerTwoScoreAtom = atom<number>(0)

export const hoverAtom = atom<boolean>(false)

export const hoveredMarkerIndexAtom = atom<number | undefined>(undefined)

export const markerOnRosetteTileAtom = atom<boolean>(false)

export const messageAtom = atom<string>('')

export const messageTypeAtom = atom<Message>(Message.Normal)
