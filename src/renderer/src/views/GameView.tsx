import { GameStage } from './constants/types'
import { GameEndView } from './GameEndView'
import { GameMiddleView } from './GameMiddleView'
import { GameStartView } from './GameStartView'
import { useGameView } from './hook/useGameView'

const GameView = () => {
  const {
    rollDice,
    turn,
    stage,
    TILES,
    tilesRef,
    markersLightRef,
    markersDarkRef,
    oneOuterRef,
    twoOuterRef,
    handleMouseIn,
    handleMouseOut,
    moveMarker,
    playerOneScore,
    playerTwoScore,
    message,
    messageType,
    match,
    play,
    playerOneDiceRoll,
    playerTwoDiceRoll
  } = useGameView()

  return (
    <div>
      {stage === GameStage.Start && (
        <GameStartView
          onRollDice={rollDice}
          turn={turn}
          message={message}
          messageType={messageType}
          onMatch={match}
          onPlay={play}
          playerOneDiceRoll={playerOneDiceRoll}
          playerTwoDiceRoll={playerTwoDiceRoll}
        />
      )}
      {stage === GameStage.Middle && (
        <GameMiddleView
          onRollDice={rollDice}
          turn={turn}
          tiles={TILES}
          tilesRef={tilesRef}
          markersLightRef={markersLightRef}
          markersDarkRef={markersDarkRef}
          oneOuterRef={oneOuterRef}
          twoOuterRef={twoOuterRef}
          onHandleMouseIn={handleMouseIn}
          onHandleMouseOut={handleMouseOut}
          onMoveMarker={moveMarker}
          playerOneDiceRoll={playerOneDiceRoll}
          playerTwoDiceRoll={playerTwoDiceRoll}
          playerOneScore={playerOneScore}
          playerTwoScore={playerTwoScore}
        />
      )}
      {stage === GameStage.End && (
        <GameEndView playerOneScore={playerOneScore} playerTwoScore={playerTwoScore} />
      )}
    </div>
  )
}
export default GameView
