import { MutableRefObject } from 'react'

export type TilesRef = MutableRefObject<HTMLDivElement[]>

export enum TileType {
  Rosette = 'ROSETTE ',
  Dots = 'DOTS',
  Eyes = 'EYES',
  Squares = 'SQUARES',
  Zebras = 'ZEBRAS',
  Empty = 'EMPTY'
}

export type TileElement = {
  id: number
  type: TileType
}

export type DiceRoll = {
  draw: number[]
  total: number | null
}

export enum GameStage {
  Start = 'START',
  Middle = 'MIDDLE',
  End = 'END'
}

export type MarkerPosition = { markerIndex: number; tileIndex: number }

export enum Message {
  Normal = 'normal',
  Important = 'important',
  Action = 'action'
}

export type ToastMessageVariant =
  | 'markerOnRosette'
  | 'rollZero'
  | 'noMoveAvailable'
  | 'markerDown'
  | 'score'
