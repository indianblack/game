import { TileElement, TileType } from './types'

export const ROSETTE_TILES: number[] = [0, 6, 11, 16, 22]

export const MIDDLE_ROSETTE = 11

export const TILES: TileElement[] = [
  {
    id: 0,
    type: TileType.Rosette
  },
  {
    id: 1,
    type: TileType.Eyes
  },
  {
    id: 2,
    type: TileType.Dots
  },
  {
    id: 3,
    type: TileType.Eyes
  },
  {
    id: 4,
    type: TileType.Empty
  },
  {
    id: 5,
    type: TileType.Empty
  },
  {
    id: 6,
    type: TileType.Rosette
  },
  {
    id: 7,
    type: TileType.Zebras
  },
  {
    id: 8,
    type: TileType.Squares
  },
  {
    id: 9,
    type: TileType.Dots
  },
  {
    id: 10,
    type: TileType.Zebras
  },
  {
    id: 11,
    type: TileType.Rosette
  },
  {
    id: 12,
    type: TileType.Dots
  },
  {
    id: 13,
    type: TileType.Zebras
  },
  {
    id: 14,
    type: TileType.Eyes
  },
  {
    id: 15,
    type: TileType.Dots
  },
  {
    id: 16,
    type: TileType.Rosette
  },
  {
    id: 17,
    type: TileType.Eyes
  },
  {
    id: 18,
    type: TileType.Dots
  },
  {
    id: 19,
    type: TileType.Eyes
  },
  {
    id: 20,
    type: TileType.Empty
  },
  {
    id: 21,
    type: TileType.Empty
  },
  {
    id: 22,
    type: TileType.Rosette
  },
  {
    id: 23,
    type: TileType.Zebras
  }
]
