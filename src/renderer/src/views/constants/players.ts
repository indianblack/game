export enum Player {
  One = 'PLAYER_ONE',
  Two = 'PLAYER_TWO'
}

export const TILES_PLAYER_ONE: number[] = [4, 3, 2, 1, 0, 8, 9, 10, 11, 12, 13, 14, 15, 7, 6]

export const PLAYER_ONE_LAST_TILE = 6

export const TILES_PLAYER_TWO: number[] = [20, 19, 18, 17, 16, 8, 9, 10, 11, 12, 13, 14, 15, 23, 22]

export const PLAYER_TWO_LAST_TILE = 22
