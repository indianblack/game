import { styled } from '../../theme'
import { Text } from '../../components'

const WIDTH = '558px'
const HEIGHT = '402px'

const CardBox = styled('div', {
  display: 'grid',
  gridTemplateColumns: 'repeat(2, 168px)',
  gridTemplateRows: 'repeat(1, 196px)',
  gridColumnGap: '$5',
  padding: '$6',
  borderRadius: '$1'
})

const Card = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: '$1',
  [`& > ${Text}`]: {
    fontSize: '$2'
  },
  variants: {
    active: {
      true: {
        backgroundColor: '$gray40'
      },
      false: {
        backgroundColor: '$gray20'
      }
    }
  }
})

const MessageBox = styled('div', {
  display: 'flex',
  flexDirection: 'row',
  gap: '$5',
  alignItems: 'center',
  justifyContent: 'center',
  [`& > ${Text}`]: {
    fontSize: '$2'
  }
})

export { WIDTH, HEIGHT, CardBox, Card, MessageBox }
