import {
  Badge,
  Border,
  Box,
  Button,
  CornerDecoration,
  Flex,
  Layout,
  MarkerBox,
  Section,
  Text
} from '../../components'
import { Player } from '../constants/players'
import { DiceRoll, Message } from '../constants/types'
import { HEIGHT, WIDTH, CardBox, Card, MessageBox } from './styles'

type StartGameViewProps = {
  onRollDice: () => void
  turn: string
  message: string
  messageType: Message
  onMatch: () => void
  onPlay: () => void
  playerOneDiceRoll: DiceRoll | undefined
  playerTwoDiceRoll: DiceRoll | undefined
}

const GameStartView = ({
  onRollDice,
  turn,
  message,
  messageType,
  onMatch,
  onPlay,
  playerOneDiceRoll,
  playerTwoDiceRoll
}: StartGameViewProps) => {
  const match = playerOneDiceRoll?.total === playerTwoDiceRoll?.total
  return (
    <Layout>
      <Border
        css={{
          width: WIDTH,
          height: HEIGHT,
          minWidth: WIDTH,
          minHeight: HEIGHT
        }}
      >
        <Box
          css={{
            width: WIDTH,
            height: HEIGHT,
            minWidth: WIDTH,
            minHeight: HEIGHT
          }}
        >
          {/* Title */}
          <Section
            border="bottom"
            css={{
              width: WIDTH,
              minWidth: WIDTH
            }}
          >
            <CornerDecoration border="right" background="image" height="static" />
            <Flex>
              <Text size={4}>The royal game of Ur</Text>
            </Flex>
            <CornerDecoration border="left" background="image" height="static" />
          </Section>
          <Flex>
            <CornerDecoration border="right" height="max" />
            <Flex>
              <CardBox>
                {/* Player one */}
                <Card active={turn === Player.One && !match}>
                  <MarkerBox player="light" />
                  <Text css={{ marginBottom: '$3' }}>Player light</Text>
                  {playerOneDiceRoll === undefined && (
                    <Button
                      onClick={onRollDice}
                      disabled={turn === Player.Two}
                      css={{ width: '120px' }}
                    >
                      Roll
                    </Button>
                  )}
                  {playerOneDiceRoll !== undefined && (
                    <Badge color="light" css={{ width: '120px' }}>
                      <Text>Score: {playerOneDiceRoll.total}</Text>
                    </Badge>
                  )}
                </Card>
                {/* Player two */}
                <Card active={turn === Player.Two && !match}>
                  <MarkerBox player="dark" />
                  <Text css={{ marginBottom: '$3' }}>Player dark</Text>
                  {playerTwoDiceRoll === undefined && (
                    <Button
                      onClick={onRollDice}
                      disabled={turn === Player.One}
                      css={{ width: '120px' }}
                    >
                      Roll
                    </Button>
                  )}
                  {playerTwoDiceRoll !== undefined && (
                    <Badge color="light" css={{ width: '120px' }}>
                      <Text>Score: {playerTwoDiceRoll.total}</Text>
                    </Badge>
                  )}
                </Card>
              </CardBox>
            </Flex>
            <CornerDecoration border="left" height="max" />
          </Flex>
          {/* Footer */}
          <Section
            border="top"
            css={{
              width: WIDTH,
              minWidth: WIDTH
            }}
          >
            <CornerDecoration border="right" background="image" height="static" />
            <Flex>
              <MessageBox>
                <Text type={messageType}>{message}</Text>
                {messageType === 'important' && (
                  <Button onClick={onMatch} css={{ width: '95px' }}>
                    Ok
                  </Button>
                )}
                {messageType === 'action' && (
                  <Button onClick={onPlay} css={{ width: '95px' }}>
                    Play
                  </Button>
                )}
              </MessageBox>
            </Flex>
            <CornerDecoration border="left" background="image" height="static" />
          </Section>
        </Box>
      </Border>
    </Layout>
  )
}
export default GameStartView
