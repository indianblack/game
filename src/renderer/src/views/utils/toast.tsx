import { toast } from 'react-toastify'
import { Player } from '../constants/players'
import { ToastMessageVariant } from '../constants/types'

// Toast notifications
export const notify = (player: Player, messageVariant: ToastMessageVariant) => {
  const playerName = player === Player.One ? 'Player light' : 'Player dark'
  const toastPosition =
    player === Player.One ? toast.POSITION.BOTTOM_LEFT : toast.POSITION.BOTTOM_RIGHT
  const customClass = player === Player.One ? 'playerOne' : 'playerTwo'
  const customImportantClass = 'important'

  switch (messageVariant) {
    case 'markerOnRosette': {
      toast(`${playerName} your counter stands on rosette tile. You have one additional move.`, {
        position: toastPosition,
        theme: 'dark',
        className: customClass
      })
      break
    }
    case 'rollZero': {
      toast(`${playerName} you rolled a zero. No move for you!`, {
        position: toastPosition,
        theme: 'dark',
        className: customClass
      })
      break
    }
    case 'noMoveAvailable': {
      toast(`${playerName} due to unfavorable placement none of your counters could move.`, {
        position: toastPosition,
        theme: 'dark',
        className: customClass
      })
      break
    }
    case 'markerDown': {
      toast(
        `${playerName} one of your counters was sent off the board and must start again from the beginning.`,
        {
          position: toastPosition,
          theme: 'dark',
          className: customImportantClass
        }
      )
      break
    }
    case 'score': {
      toast(`${playerName} one of your counters finished the board. Good job! Score +1`, {
        position: toastPosition,
        theme: 'dark',
        className: customClass
      })
      break
    }
  }
}
