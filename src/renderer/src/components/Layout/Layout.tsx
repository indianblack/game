import { styled } from '../../theme'

const Layout = styled('div', {
  display: 'flex',
  flexDirection: 'row',
  minHeight: '100vh',
  minWidth: '100vw',
  height: '100%',
  width: '100%',
  justifyContent: 'center',
  alignItems: 'center'
})

export default Layout
