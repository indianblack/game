import { styled } from '../../theme'
import MarkerLight from '../../assets/marker.light.png'
import MarkerDark from '../../assets/marker.dark.png'

const MarkerBox = styled('div', {
  display: 'flex',
  flexDirection: 'row',
  width: '40px',
  minWidth: '40px',
  maxWidth: '40px',
  height: '40px',
  minHeight: '40px',
  maxHeight: '40px',
  margin: '$6',
  borderRadius: '50%',
  boxShadow: '-1px 4px 20px rgba(11, 26, 54, 0.15), -0.25px 2px 2px rgba(0, 0, 0, 0.7)',
  variants: {
    player: {
      light: {
        backgroundImage: `url(${MarkerLight})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover'
      },
      dark: {
        backgroundImage: `url(${MarkerDark})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover'
      }
    }
  }
})

export default MarkerBox
