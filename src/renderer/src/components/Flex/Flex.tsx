import { styled } from '../../theme'

const Flex = styled('div', {
  display: 'flex',
  flex: '1',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center'
})

export default Flex
