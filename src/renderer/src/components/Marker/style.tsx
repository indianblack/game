import { styled } from '../../theme'
import MarkerLight from '../../assets/marker.light.png'
import MarkerDark from '../../assets/marker.dark.png'

const MarkerButton = styled('button', {
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'center',
  border: 'none',
  borderRadius: '50%',
  width: '40px',
  height: '40px',
  maxWidth: '40px',
  maxHeight: '40px',
  outline: 'none',
  cursor: 'pointer',
  transition: '250ms',
  boxShadow: '-1px 4px 20px rgba(11, 26, 54, 0.15), -0.25px 2px 2px rgba(0, 0, 0, 0.7)',
  '&:hover': {
    boxShadow: '0px 4px 20px rgba(11, 26, 54, 0.3), 0px 2px 4px rgba(0, 0, 0, 0.9)',
    borderWidth: '0.5px',
    borderColor: '$gray20',
    borderStyle: 'solid',
    borderRadius: '50%',
    transition: '250ms'
  },
  variants: {
    color: {
      light: {
        backgroundImage: `url(${MarkerLight})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundColor: '$white',
        borderWidth: '0.5px',
        borderColor: '$white',
        borderStyle: 'solid',
        borderRadius: '50%'
      },
      dark: {
        backgroundImage: `url(${MarkerDark})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundColor: '$blue',
        borderWidth: '0.5px',
        borderColor: '$blue',
        borderStyle: 'solid',
        borderRadius: '50%'
      }
    }
  }
})

export { MarkerButton }
