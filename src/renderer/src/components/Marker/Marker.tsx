import { MarkerButton } from './style'

type MarkerProps = {
  index: number
  disabled: boolean
  markersRef: React.MutableRefObject<HTMLButtonElement[]>
  onHandleMouseIn: (hoveredMarkerIndex: number) => void
  onHandleMouseOut: () => void
  onMoveMarker: (clickedMarkerIndex: number) => void
  color: 'light' | 'dark'
}

const Marker = ({
  index,
  disabled,
  markersRef,
  onHandleMouseIn,
  onHandleMouseOut,
  onMoveMarker,
  color
}: MarkerProps) => {
  return (
    <MarkerButton
      color={color}
      style={{ width: '50px', height: '50px' }}
      ref={(ref) => {
        if (ref) markersRef.current[index] = ref
      }}
      disabled={disabled}
      onMouseOver={() => {
        if (disabled) return // If other player turn - don't add hover effect
        onHandleMouseIn(index)
      }}
      onMouseOut={onHandleMouseOut}
      onClick={() => onMoveMarker(index)}
    />
  )
}

export default Marker
