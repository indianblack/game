import { styled } from '../../theme'

const GAME_BASIS_WIDTH = '212px'
const GAME_BASIS_HEIGHT = '552px'

const GameWrapper = styled('div', {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  width: '212px',
  height: '552px',
  position: 'relative'
})

const GameBasis = styled('div', {
  display: 'grid',
  gridAutoFlow: 'column',
  gridTemplateRows: 'repeat(8, 60px)',
  gridTemplateColumns: 'repeat(3, 60px)',
  gridColumnGap: '$3',
  gridRowGap: '$3',
  zIndex: '$containedField'
})

const GameContainerWrapper = styled('div', {
  position: 'absolute',
  zIndex: '0',
  top: 0,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start',
  alignItems: 'center',
  width: GAME_BASIS_WIDTH,
  height: GAME_BASIS_HEIGHT
})

const GameContainerTop = styled('div', {
  width: '212px',
  height: '280px',
  backgroundColor: '$gray',
  borderRadius: '$1',
  boxShadow:
    '0px 3px 0px 2px rgba(14, 14, 15, 1), 0px 14px 0px 2px rgba(14, 14, 15, 1), -4px 8px 24px rgba(0, 0, 0, 0.65), -4px 12px 64px rgba(30, 33, 40, 0.3)'
})

const GameContainerMiddle = styled('div', {
  width: '76px',
  height: '128px',
  backgroundColor: '$gray',
  boxShadow: '-4px 8px 24px rgba(0, 0, 0, 0.65), -4px 12px 64px rgba(30, 33, 40, 0.3)'
})

const GameContainerBottom = styled('div', {
  width: '212px',
  height: '144px',
  backgroundColor: '$gray',
  borderRadius: '$1',
  boxShadow:
    '0px 3px 0px 2px rgba(14, 14, 15, 1), 0px 14px 0px 2px rgba(14, 14, 15, 1), -4px 8px 24px rgba(0, 0, 0, 0.65), -4px 12px 64px rgba(30, 33, 40, 0.3)'
})

export {
  GameWrapper,
  GameBasis,
  GameContainerTop,
  GameContainerWrapper,
  GameContainerMiddle,
  GameContainerBottom
}
