import {
  GameContainerBottom,
  GameContainerMiddle,
  GameContainerTop,
  GameContainerWrapper
} from './style'

const GameContainer = () => {
  return (
    <GameContainerWrapper>
      <GameContainerTop />
      <GameContainerMiddle />
      <GameContainerBottom />
    </GameContainerWrapper>
  )
}
export default GameContainer
