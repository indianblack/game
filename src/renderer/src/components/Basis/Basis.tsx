import { TileElement, TilesRef } from '../../views/constants/types'
import Tile from '../Tile/Tile'
import GameContainer from './GameContainer'
import { GameBasis, GameWrapper } from './style'

type BasisProps = {
  tiles: TileElement[]
  tilesRef: TilesRef
}

const Basis = ({ tiles, tilesRef }: BasisProps) => {
  return (
    <GameWrapper>
      <GameBasis>
        {tiles.map((el, i) => {
          return <Tile key={i} index={i} id={el.id} type={el.type} tilesRef={tilesRef} />
        })}
      </GameBasis>
      <GameContainer />
    </GameWrapper>
  )
}

export default Basis
