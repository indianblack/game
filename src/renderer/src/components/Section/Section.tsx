import { styled } from '../../theme'

const Section = styled('div', {
  height: '71px',
  minHeight: '71px',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  variants: {
    border: {
      top: {
        borderTop: '0.5px solid $beige'
      },
      bottom: {
        borderBottom: '0.5px solid $beige'
      }
    }
  }
})

export default Section
