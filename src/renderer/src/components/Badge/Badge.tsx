import { styled } from '../../theme'
import { Text } from '../Text'

const Badge = styled('div', {
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'center',
  whiteSpace: 'nowrap',
  border: 'none',
  borderRadius: '$1',
  minWidth: '103px',
  height: '40px',
  minHeight: '40px',
  [`${Text}`]: {
    color: '$font-primary',
    textTransform: 'uppercase',
    fontSize: '$1'
  },
  variants: {
    color: {
      light: {
        backgroundColor: '$white90'
      },
      dark: {
        backgroundColor: '$gray80',
        [`${Text}`]: {
          color: '$white'
        }
      }
    }
  }
})

export default Badge
