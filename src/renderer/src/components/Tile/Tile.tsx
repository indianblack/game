import { TilesRef, TileType } from '../../views/constants/types'
import { TileContainer } from './style'

type TileContainerType = 'rosette' | 'dots' | 'eyes' | 'squares' | 'zebras' | 'empty'

const deriveType = (type: TileType): TileContainerType => {
  switch (type) {
    case TileType.Rosette: {
      return 'rosette'
    }
    case TileType.Dots: {
      return 'dots'
    }
    case TileType.Eyes: {
      return 'eyes'
    }
    case TileType.Squares: {
      return 'squares'
    }
    case TileType.Zebras: {
      return 'zebras'
    }
    default: {
      return 'empty'
    }
  }
}

type TileProps = {
  index: number
  id: number
  type: TileType
  tilesRef: TilesRef
}

const Tile = ({ index, id, type, tilesRef }: TileProps) => {
  return (
    <TileContainer
      id={id.toString()}
      type={deriveType(type)}
      ref={(ref) => {
        if (ref) tilesRef.current[index] = ref
      }}
    />
  )
}
export default Tile
