import { styled } from '../../theme'
import Rosette from '../../assets/rosette.png'
import Dots from '../../assets/dots.png'
import Eyes from '../../assets/eyes.png'
import Squares from '../../assets/squares.png'
import Zebras from '../../assets/zebras.png'

const TileContainer = styled('div', {
  width: '60px',
  height: '60px',
  backgroundColor: '$white',
  borderRadius: '$1',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  variants: {
    type: {
      rosette: {
        backgroundImage: `url(${Rosette})`
      },
      dots: {
        backgroundImage: `url(${Dots})`
      },
      eyes: {
        backgroundImage: `url(${Eyes})`
      },
      squares: {
        backgroundImage: `url(${Squares})`
      },
      zebras: {
        backgroundImage: `url(${Zebras})`
      },
      empty: {
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'flex-end'
      }
    }
  },
  defaultVariants: {
    type: 'empty'
  }
})

export { TileContainer }
