import { styled } from '../../theme'

const Border = styled('div', {
  borderWidth: '0.5px',
  borderColor: '$beige',
  borderStyle: 'solid'
})

export default Border
