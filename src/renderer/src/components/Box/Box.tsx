import { styled } from '../../theme'

const Box = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  borderRadius: '$3',
  backgroundColor: '$white80'
})

export default Box
