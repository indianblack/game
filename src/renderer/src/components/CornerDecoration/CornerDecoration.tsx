import { styled } from '../../theme'
import CornerImage from '../../assets/corner.image.png'

const CornerDecoration = styled('div', {
  display: 'flex',
  flexDirection: 'row',
  width: '67px',
  minWidth: '67px',
  maxWidth: '67px',
  variants: {
    border: {
      right: {
        borderRight: '0.5px solid $beige'
      },
      left: {
        borderLeft: '0.5px solid $beige'
      }
    },
    background: {
      image: {
        backgroundImage: `url(${CornerImage})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: '45px 45px'
      }
    },
    height: {
      static: {
        height: '71px',
        minHeight: '71px'
      },
      max: {
        height: '100%',
        minHeight: '100%'
      }
    }
  }
})

export default CornerDecoration
