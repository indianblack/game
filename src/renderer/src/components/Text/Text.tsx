import { CSS } from '@stitches/react'
import { config, styled } from '../../theme'

const FontSizeArray = [0, 1, 2, 3, 4] as const
type FontSize = typeof FontSizeArray[number]

const fsize = () => {
  const retv: Record<number, CSS<typeof config>> = {}
  for (let i = 0; i < FontSizeArray.length; i++) {
    retv[i as number] = { fontSize: `$${i}` }
  }
  return retv as Record<FontSize, CSS<typeof config>>
}

const Text = styled('span', {
  fontFamily: "'Stylish',sans-serif",
  fontWeight: 400,
  variants: {
    size: fsize(),
    fontWeight: {
      400: { fontWeight: '$400' }
    },
    textAlign: {
      left: { textAlign: 'left' },
      right: { textAlign: 'right' },
      center: { textAlign: 'center' }
    },
    whiteSpace: {
      nowrap: { whiteSpace: 'nowrap' },
      'pre-line': { whiteSpace: 'pre-line' }
    },
    color: {
      'font-primary': {
        color: '$font-primary'
      },
      white: {
        color: '$white'
      }
    },
    case: {
      uppercase: { textTransform: 'uppercase' }
    },
    type: {
      normal: {
        color: '$font-primary'
      },
      important: {
        color: '$red'
      },
      action: {
        color: '$blue'
      }
    }
  },
  defaultVariants: {
    size: 0,
    color: 'font-primary',
    case: 'uppercase'
  }
})

export default Text
