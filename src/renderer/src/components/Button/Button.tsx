import { ComponentProps, PropsWithChildren } from 'react'
import { styled } from '../../theme'
import { Text } from '../Text'

const MainButton = styled('button', {
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'center',
  whiteSpace: 'nowrap',
  border: 'none',
  borderRadius: '$1',
  transition: '250ms',
  outline: 'none !important',
  width: '100%',
  height: '40px',
  minHeight: '40px',
  padding: '$2',
  cursor: 'pointer',
  boxShadow: '-1px 2px 20px rgba(0, 0, 0, 0.15), -0.25px 1px 2px rgba(0, 0, 0, 0.8)',
  '&[disabled]': {
    opacity: 0.5,
    cursor: 'initial',
    pointerEvents: 'initial',
    boxShadow: 'none',
    transition: '250ms',
    '&:hover': {
      boxShadow: 'none'
    }
  },
  '&:hover': {
    transition: '250ms',
    boxShadow: '-1px 2px 20px rgba(0, 0, 0, 0.2), -0.25px 2px 5px rgba(0, 0, 0, 0.8)'
  },
  [`${Text}`]: {
    color: 'inherit',
    textTransform: 'uppercase',
    fontSize: '$1'
  },
  variants: {
    variant: {
      solid: {
        backgroundColor: '$white'
      }
    }
  },
  defaultVariants: {
    variant: 'solid'
  }
})

const InnerBorder = styled('div', {
  borderWidth: '0.5px',
  borderColor: '$beige',
  borderStyle: 'solid',
  borderRadius: '$1',
  width: '100%',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
})

interface ButtonProps extends ComponentProps<typeof MainButton> {
  variant?: 'solid'
  onClick: () => void
  disabled?: boolean
}

const Button = ({
  children,
  variant,
  onClick,
  disabled = false,
  css
}: PropsWithChildren<ButtonProps>) => {
  return (
    <MainButton onClick={onClick} variant={variant} disabled={disabled} css={css}>
      <InnerBorder>
        <Text>{children}</Text>
      </InnerBorder>
    </MainButton>
  )
}

export default Button
