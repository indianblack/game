import { Provider } from 'jotai'
import { ToastContainer } from 'react-toastify'
import { globalStyles, Root, theme } from './theme'
import GameView from './views/GameView'

function App() {
  globalStyles()
  return (
    <Provider>
      <div className={theme}>
        <Root>
          <ToastContainer />
          <GameView />
        </Root>
      </div>
    </Provider>
  )
}

export default App
