import { createStitches } from '@stitches/react'

export const { styled, css, globalCss, keyframes, getCssText, theme, createTheme, config } =
  createStitches({
    theme: {
      space: {
        0: 0,
        1: '2px',
        2: '4px',
        3: '8px',
        4: '12px',
        5: '16px',
        6: '24px',
        7: '32px',
        8: '48px',
        9: '64px'
      },
      fontSizes: {
        0: '16px',
        1: '18px',
        2: '20px',
        3: '22px',
        4: '24px'
      },
      radii: {
        0: '0px',
        1: '5px',
        2: '10px',
        3: '20px'
      },
      zIndices: {
        containedField: 2
      },
      colors: {
        beige: 'rgba(165, 159, 147, 1)',
        beige90: 'rgba(165, 159, 147, 0.9)',
        white: 'rgba(251, 247, 238, 1)',
        white80: 'rgba(251, 247, 238, 0.8)',
        white90: 'rgba(251, 247, 238, 0.9)',
        blue: 'rgba(23, 48, 104, 1)',
        blue80: 'rgba(23, 48, 104, 0.8)',
        blue90: 'rgba(23, 48, 104, 0.9)',
        red: 'rgba(170, 50, 60, 1)',
        red90: 'rgba(170, 50, 60, 0.9)',
        gray: 'rgba(40, 42, 46, 1)',
        gray20: 'rgba(40, 42, 46, 0.2)',
        gray40: 'rgba(40, 42, 46, 0.4)',
        gray80: 'rgba(40, 42, 46, 0.8)',
        black: 'rgba(14, 14, 15, 1)',
        black90: 'rgba(14, 14, 15, 0.9)',
        'font-primary': '$gray'
      }
    }
  })
