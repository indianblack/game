import Background from '../assets/background.png'
import { globalCss, styled } from './theme'
import StylishRegular from '../assets/fonts/Stylish-Regular.ttf'
import 'react-toastify/dist/ReactToastify.min.css'

export const Root = styled('div', {
  display: 'flex',
  flexDirection: 'row',
  height: '100%',
  minHeight: '100vh',
  width: '100%',
  minWidth: '100vw',
  backgroundImage: `url(${Background})`,
  backgroundRepeat: 'no-repeat',
  backgroundAttachment: 'fixed',
  backgroundPosition: 'center',
  backgroundSize: 'cover'
})

export const globalStyles = globalCss({
  '@font-face': [
    {
      fontFamily: 'Stylish',
      fontWeight: 400,
      fontDisplay: 'swap',
      src: `local('Stylish'), url(${StylishRegular}) format('woff2')`
    }
  ],

  // Based off https://github.com/sindresorhus/modern-normalize/blob/main/modern-normalize.css
  html: {
    lineHeight: 1.15,
    tabSize: 4,
    '-webkit-text-size-adjust': '100%',
    '-moz-tab-size': 4
  },
  body: {
    margin: 0,
    minHeight: '100vh',
    maxHeight: '100vh',
    color: '$font-primary',
    fontSize: '$1'
  },
  '*, *:before, *:after': {
    boxSizing: 'border-box'
  },
  table: {
    textIndent: 0,
    borderColor: 'inherit'
  },
  'button, input, select, textarea': {
    fontFamily: 'inherit',
    fontSize: '100%',
    lineHeight: '100%',
    margin: 0
  },
  'button, select': {
    textTransform: 'none'
  },
  '::-moz-focus-inner': {
    borderStyle: 'none',
    padding: 0
  },
  ':-moz-ui-invalid': {
    boxShadow: 'none'
  },
  "button, [type='button'], [type='reset'], [type='submit']": {
    '-webkit-appearance': 'button'
  },

  // App custom styles
  '::-webkit-scrollbar': {
    width: 14,
    height: 14,
    scrollbarGutter: 'stable'
  },
  '::-webkit-scrollbar-thumb': {
    border: '4px solid rgba(0, 0, 0, 0)',
    backgroundClip: 'padding-box',
    borderRadius: '9999px',
    backgroundColor: '$background400'
  },
  a: {
    color: 'inherit',
    fill: 'inherit',
    textDecoration: 'none'
  },
  "[role='button'], button": {
    cursor: 'pointer'
  },
  'h1, h2, h3, h4, h5, h6': {
    fontSize: 'inherit',
    fontWeight: 'inherit'
  },
  '#root': {
    fontFamily: "'Stylish', sans-serif",
    overflow: 'hidden',
    fontSize: '$1',
    color: '$font-primary'
  },
  'h1, h2, h3, h4, h5, h6, p, button, div': {
    margin: 0,
    fontFamily: "'Stylish', sans-serif"
  },
  ul: {
    listStyle: 'none',
    margin: 0,
    padding: 0
  },
  '*, ::before, ::after': {
    borderWidth: 0,
    orderStyle: 'solid'
  },

  // Override toast css classes
  '.Toastify__toast-container--bottom-left': {
    bottom: '60px !important'
  },

  '.Toastify__toast-container--bottom-right': {
    bottom: '60px !important'
  },

  '.Toastify__toast-theme--dark': {
    backgroundColor: '$black90 !important'
  },

  '.playerTwo > .Toastify__progress-bar-theme--dark': {
    backgroundColor: '$blue90 !important'
  },

  '.playerOne > .Toastify__progress-bar-theme--dark': {
    backgroundColor: '$beige90 !important'
  },

  '.important > .Toastify__progress-bar-theme--dark': {
    backgroundColor: '$red90 !important'
  }
})
